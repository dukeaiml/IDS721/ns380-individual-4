# Individual Project 4

Stock price averager - given a date range and a list of stock tickers, get the average stock price across the date ranges for all the given stocks.

## Build Process

This repository has 3 sub-repos inside it. Each sub-repo has a lambda that was built in a similar way. First, run
```
cargo lambda new <map/process/reduce>_lambda
```

Then, fill out the Request and Response structs, and the logic in the `function_handler` function. Specifically for the reduce lambda, it is important to recognize that the input to this function is not a single request but a list of requests from the prior fanned out step - therefore the input to the `function_handler` function needs to be changed for that lambda.

Once the logic is set up, the lambdas can be deployed using
```
cargo lambda build
cargo lambda deploy
```
in each of the lambda subdirectories.

Once all of the lambdas are deployed, copy the ARNs for the lambdas and keep them aside. Set up the Step Function by creating a new step function and filling out the code with the JSON found in `step_function.JSON`. You also need to create a new IAM role for the step function but there is a basic template for step functions when going through the IAM wizard that works perfectly well for this use case.

After setting this up, the step function should be fully set up and you can execute runs from either the AWS console or the CLI.

## Usage

To use this you need to start a new execution in AWS Step Functions. Here is a screenshot of me starting a new function:
![alt text](image.png)

Once you've set this up, click "Start Execution". This will take you to a page like
![alt text](image-1.png)

where you can see more about the execution of the step function.

There is a lot of detail that is easier to understand by watching the demo video available at this link: https://youtu.be/-zd8HDhVjRo

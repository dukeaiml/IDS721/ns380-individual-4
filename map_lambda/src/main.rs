use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use tracing::{debug, error};
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use chrono::{Local, NaiveDate};

#[derive(Deserialize)]
pub struct Request {
    pub start: String,
    pub end: String,
    pub tickers: String,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    payload: Vec<ResponseBody>,
}

#[derive(Serialize)]
struct ResponseBody {
    pub start: String,
    pub end: String,
    pub ticker: String,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let item = event.payload;
    let mut out: Vec<ResponseBody> = Vec::new();
    // Validate that the start date is actually a real date
    let start = match NaiveDate::parse_from_str(item.start.as_str(), "%Y-%m-%d") {
        Ok(date) => date,
        _ => {
            error!(input=item.start.as_str(), "Got badly formatted start date input");
            let resp = Response {
                req_id: event.context.request_id,
                payload: out,
            };
            return Ok(resp);
        }
    };
    debug!("Successfully parsed start date");
    
    // Validate that the end date is actually a real date
    let end = match NaiveDate::parse_from_str(item.end.as_str(), "%Y-%m-%d") {
        Ok(date) => date,
        _ => {
            error!(input=item.end.as_str(), "Got badly formatted end date input");
            let resp = Response {
                req_id: event.context.request_id,
                payload: out,
            };
            return Ok(resp);
        }
    };
    debug!("Successfully parsed start date");

    // Validate that the end date is today or in the past
    let today = Local::now().date_naive();
    if today < end {
        error!(input=item.end.as_str(), "Got too far in the future end date input");
        let resp = Response {
            req_id: event.context.request_id,
            payload: out,
        };
        return Ok(resp);
    }

    // Validate that the end date is in the future compared to the start date
    if end < start {
        error!(start=item.start.as_str(), end=item.end.as_str(), "Got end date before start date");
        let resp = Response {
            req_id: event.context.request_id,
            payload: out,
        };
        return Ok(resp);
    }
    debug!("Got valid start and end dates");

    let tickers = item.tickers.split(",");
    for t in tickers {
        out.push(ResponseBody{
            start: item.start.clone(),
            end: item.end.clone(),
            ticker: t.to_string(),
        });
    }
    
    // Return something that implements IntoResponse.
    // It will be serialized to the right response event automatically by the runtime
    let resp = Response {
        req_id: event.context.request_id,
        payload: out,
    };
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        // add json formatting
        .json()
        .init();

    run(service_fn(function_handler)).await
}

use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
pub struct Request {
    pub req_id: String,
    pub payload: String,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    payload: String,
}

async fn function_handler(event: LambdaEvent<Vec<Request>>) -> Result<Response, Error> {
    let items = event.payload;

    let mut total: f64 = 0.0;
    let mut num_avgs: f64 = 0.0;
    for avg_price in items {
        let avg: f64 = avg_price.payload.parse().unwrap();
        total += avg;
        num_avgs += 1.0;
    }
    
    let resp = Response {
        req_id: event.context.request_id,
        payload: format!("Average price: {}", total/num_avgs).to_string(),
    };
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        // add json formatting
        .json()
        .init();

    run(service_fn(function_handler)).await
}

use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use tracing::{info, debug, error};
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use chrono::NaiveDate;
use time::OffsetDateTime;
use yahoo_finance_api::YahooConnector;

#[derive(Deserialize)]
pub struct Request {
    pub start: String,
    pub end: String,
    pub ticker: String,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    payload: String,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let item = event.payload;

    // Validate that the start date is actually a real date
    let start = match NaiveDate::parse_from_str(item.start.as_str(), "%Y-%m-%d") {
        Ok(date) => date,
        Err(err) => {
            error!(input=item.start.as_str(), "Got badly formatted start date input");
            let resp = Response {
                req_id: event.context.request_id,
                payload: ("Please make sure your body is formatted as {'start': 'YYYY-MM-DD','end': 'YYYY-MM-DD','ticker': '<TICKER>'} ".to_string() + &err.to_string() + "\n"),
            };
            return Ok(resp);
        }
    };
    debug!("Successfully parsed start date");
    
    // Validate that the end date is actually a real date
    let end = match NaiveDate::parse_from_str(item.end.as_str(), "%Y-%m-%d") {
        Ok(date) => date,
        Err(err) => {
            error!(input=item.end.as_str(), "Got badly formatted end date input");
            let resp = Response {
                req_id: event.context.request_id,
                payload: ("Please make sure your body is formatted as {'start': 'YYYY-MM-DD','end': 'YYYY-MM-DD','ticker': '<TICKER>'} ".to_string() + &err.to_string() + "\n"),
            };
            return Ok(resp);
        }
    };
    debug!("Successfully parsed start date");

    let provider = YahooConnector::new();
    // Beginning of start date
    let start_dt = OffsetDateTime::from_unix_timestamp(start.and_hms_opt(0, 0, 0).unwrap().timestamp()).unwrap();
    // End of end date
    let end_dt = OffsetDateTime::from_unix_timestamp(end.and_hms_opt(23, 59, 59).unwrap().timestamp()).unwrap();
    info!(ticker = item.ticker.as_str(), start = start.format("%Y-%m-%d").to_string(), end = end.format("%Y-%m-%d").to_string(), "Getting stock price information");
    let resp = provider.get_quote_history(item.ticker.as_str(), start_dt, end_dt).await?;
    let mut total: f64 = 0.0;
    let mut num_quotes: f64 = 0.0;
    for quote in resp.quotes().unwrap().iter() {
        total += quote.adjclose;
        num_quotes += 1.0;
    }

    debug!("Returning quotes");
    // Return something that implements IntoResponse.
    // It will be serialized to the right response event automatically by the runtime
    let resp = Response {
        req_id: event.context.request_id,
        payload: format!("{:.2}", total/num_quotes),
    };
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        // add json formatting
        .json()
        .init();

    run(service_fn(function_handler)).await
}
